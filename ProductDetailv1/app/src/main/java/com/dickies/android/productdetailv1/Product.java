package com.dickies.android.productdetailv1;

/**
 * Created by Phil on 28/06/2018.
 */

public class Product {

    private String mId;
    private String mTitle;
    private String mLocation;
    private String mCategory;

    public Product() {

    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }


}
