package com.dickies.android.productdetailv1;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;


public class ProductActivity extends SingleFragmentActivity {

    private static final String EXTRA_PRODUCT_ID =
            "com.dickies.android.productdetailv1.product_id";

    @Override
    protected Fragment createFragment() {
        String productId = (String) getIntent()
                .getSerializableExtra(EXTRA_PRODUCT_ID);
        return ProductFragment.newInstance(productId);
    }

    public static Intent newIntent(Context packageContext, String productId) {
        Intent intent = new Intent(packageContext, ProductActivity.class);
        intent.putExtra(EXTRA_PRODUCT_ID, productId);
        return intent;
    }




}
