package com.dickies.android.productdetailv1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Phil on 30/06/2018.
 */

public class ProductListFragment extends Fragment {

    private RecyclerView mProductRecyclerView;
    private ProductAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        mProductRecyclerView = (RecyclerView) view
                .findViewById(R.id.product_recycler_view);
        mProductRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_product_list, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }


        private void updateUI() {
        ProductLab productLab = ProductLab.get(getActivity());
        List<Product> products = productLab.getProducts();

        if (mAdapter == null) {
            mAdapter = new ProductAdapter(products);
            mProductRecyclerView.setAdapter(mAdapter);
        }else {
            mAdapter.notifyDataSetChanged();
        }

        }


        private class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


            private TextView mTitleTextView;
            private TextView mProductCategoryTextView;
            private TextView mProductLocationTextView;
            private Product mProduct;



            public ProductHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

                mTitleTextView = (TextView)
                        itemView.findViewById(R.id.list_item_product_title_text_view);
                mProductCategoryTextView = (TextView) itemView.findViewById(R.id.list_item_product_category_text_view);
                mProductLocationTextView = (TextView) itemView.findViewById(R.id.list_item_product_location_text_view);
        }


        public void bindProduct(Product product) {
            mProduct = product;
            mTitleTextView.setText(mProduct.getTitle());
            mProductCategoryTextView.setText(mProduct.getCategory().toString());
            mProductLocationTextView.setText(mProduct.getLocation());
        }

            @Override
            public void onClick(View v) {

                Intent intent = ProductPagerActivity.newIntent(getActivity(), mProduct.getId());
                startActivity(intent);


            }
    }


        private class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {

            private List<Product> mProducts;

            public ProductAdapter(List<Product> products) {
                mProducts = products;
            }

            @Override
            public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                View view = layoutInflater
                        .inflate(R.layout.list_item_product, parent, false);
                return new ProductHolder(view);
            }

            @Override
            public void onBindViewHolder(ProductHolder holder, int position) {
                Product product = mProducts.get(position);
                holder.bindProduct(product);
            }

            @Override
            public int getItemCount() {
                return mProducts.size();
            }



        }


        }






