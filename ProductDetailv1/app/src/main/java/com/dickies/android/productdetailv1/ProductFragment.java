package com.dickies.android.productdetailv1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Phil on 28/06/2018.
 */

public class ProductFragment extends Fragment {

    private static final String ARG_PRODUCT_ID = "product_id";
    private static final String DIALOG_CATEGORY = "DialogCategory";

    private static final int REQUEST_CATEGORY = 0;



    private Product mProduct;
    private EditText mTitleField;
    private Button mLocationButton;
    private Button mCategoryButton;

    public static ProductFragment newInstance(String productId) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_PRODUCT_ID, productId);

        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String productId = (String) getArguments().getSerializable(ARG_PRODUCT_ID);
        mProduct = ProductLab.get(getActivity()).getProduct(productId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product, container, false);

        mTitleField = (EditText)v.findViewById(R.id.product_title);
        mTitleField.setText(mProduct.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                    // This space intentionally left blank
            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mProduct.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mLocationButton = (Button)v.findViewById(R.id.product_location);
        mLocationButton.setText(mProduct.getLocation());
        mLocationButton.setEnabled(false);

        mCategoryButton = (Button)v.findViewById(R.id.product_category);
        updateCategory(mProduct.getCategory());
        mCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                ProductCategoryPickerFragment dialog = ProductCategoryPickerFragment
                        .newInstance(mProduct.getCategory());
                dialog.setTargetFragment(ProductFragment.this, REQUEST_CATEGORY);

                dialog.show(manager, DIALOG_CATEGORY);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_CATEGORY) {
            String category = (String) data
                    .getSerializableExtra(ProductCategoryPickerFragment.EXTRA_CATEGORY);
            mProduct.setCategory(category);
            updateCategory(mProduct.getCategory().toString());
        }
    }

    private void updateCategory(String text) {
        mCategoryButton.setText(text);
    }


}
