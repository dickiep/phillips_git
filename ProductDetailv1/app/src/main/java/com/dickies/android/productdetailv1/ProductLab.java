package com.dickies.android.productdetailv1;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phil on 29/06/2018.
 */

public class ProductLab {
    private static ProductLab sProductLab;
    private List<Product> mProducts;


    public static ProductLab get(Context context) {
        if (sProductLab == null) {
            sProductLab = new ProductLab(context);
        }
        return sProductLab;
    }

    private ProductLab (Context context) {
        mProducts = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return mProducts;
    }

    public Product getProduct(String id) {
        for (Product product : mProducts) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return null;
    }

    public void addProduct(Product p) {
        mProducts.add(p);
    }



}
