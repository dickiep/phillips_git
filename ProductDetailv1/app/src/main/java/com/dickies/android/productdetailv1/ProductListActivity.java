package com.dickies.android.productdetailv1;


import android.support.v4.app.Fragment;

/**
 * Created by Phil on 30/06/2018.
 */

public class ProductListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new ProductListFragment();
    }
}
