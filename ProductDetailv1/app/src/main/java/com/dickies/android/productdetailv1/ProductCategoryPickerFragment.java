package com.dickies.android.productdetailv1;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by Phil on 01/07/2018.
 */

public class ProductCategoryPickerFragment extends DialogFragment {

    final String[] CATEGORIES ={"Plumbing", "Marine", "Building Materials"};
    String categorySelected;
    private static final String ARG_CATEGORY = "category";

    public static final String EXTRA_CATEGORY =
            "com.dickies.android.productdetailv1.category";


    public static ProductCategoryPickerFragment newInstance(String categorySelected) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_CATEGORY, categorySelected);

        ProductCategoryPickerFragment fragment = new ProductCategoryPickerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.category_picker_title).setSingleChoiceItems(CATEGORIES, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                categorySelected = CATEGORIES[which];
                            case 1:
                                categorySelected = CATEGORIES[which];
                            case 2:
                                categorySelected = CATEGORIES[which];

                        }

                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Category Selected : "+categorySelected, Toast.LENGTH_SHORT).show();
                        String selected = categorySelected;
                        sendResult(Activity.RESULT_OK, selected);

                    }
                })
                .create();

    }

    private void sendResult(int resultCode, String categorySelected) {
        if (getTargetFragment() == null) {
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_CATEGORY, categorySelected);

        getTargetFragment()
                .onActivityResult(getTargetRequestCode(), resultCode, intent);
    }

}
